﻿using currency_converter_api.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace currency_converter_api.Services
{
    public class CurrenciesService
    {
        public ScoopCurrenciesResponse GetCurrencies()
        {
            var apiKey = Utilities.GetValue("scoopApiKey");
            var uri = $"{Utilities.GetValue("scoopApiUrl")}/currencies?api_key={apiKey}";

            var client = new RestClient(uri);
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            var res = JsonSerializer.Deserialize<ScoopCurrenciesResponse>(response.Content);
            return res;
        }
    }
}
