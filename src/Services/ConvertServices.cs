﻿using currency_converter_api.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace currency_converter_api.Services
{
    public class ConvertServices
    {
        public ScoopConvertResponse Convert(string from, string to, int amount)
        {
            var apiKey = Utilities.GetValue("scoopApiKey");
            var uri = $"{Utilities.GetValue("scoopApiUrl")}/convert?amount={amount}&from={from}&to={to}&api_key={apiKey}";

            var client = new RestClient(uri);
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            var res = JsonSerializer.Deserialize<ScoopConvertResponse>(response.Content);
            return res;
        }
    }
}
