﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace currency_converter_api.Models
{
    public class Currency
    {
        public string currency_name { get; set; }
        public string currency_code { get; set; }
        public string decimal_units { get; set; }
        public List<string> countries { get; set; }
    }
}
