﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace currency_converter_api.Models
{
    public class CurrenciesResponse
    {
        public Dictionary<string,Currency> fiats { get; set; }
    }
}
