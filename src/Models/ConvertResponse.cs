﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace currency_converter_api.Models
{
    public class ConvertResponse
    {
        public string from { get; set; }
        public string to { get; set; }
        public int amount { get; set; }
        public float value { get; set; }
    }
}
