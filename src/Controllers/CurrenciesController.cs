﻿using currency_converter_api.Models;
using currency_converter_api.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace currency_converter_api.Controllers
{
    [Route("v1/[controller]")]
    [ApiController]
    public class CurrenciesController : ControllerBase
    {
        private CurrenciesService _service = new CurrenciesService();

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_service.GetCurrencies());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace);
                return NotFound();
            }
        }
    }
}
