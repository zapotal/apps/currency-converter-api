﻿using currency_converter_api.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace currency_converter_api.Controllers
{
    [Route("v1/[controller]")]
    [ApiController]
    public class ConvertController : ControllerBase
    {
        private ConvertServices _service = new ConvertServices();

        [HttpGet("from/{from}/to/{to}/amount/{amount}")]
        public IActionResult Get(string from, string to, int amount)
        {
            try
            {
                return Ok(_service.Convert(from,to,amount));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace);
                return NotFound();
            }
        }
    }
}
